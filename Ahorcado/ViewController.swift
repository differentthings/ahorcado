//
//  ViewController.swift
//  Ahorcado
//
//  Created by DAM on 20/11/14.
//  Copyright (c) 2014 DAM. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    @IBOutlet weak var palabraLabel: UILabel!
    @IBOutlet weak var letraTextField: UITextField!
    @IBOutlet weak var imagen: UIImageView!
    
    var palabras:[[String]] = [
        ["W", "A", "R", "R", "E", "N"],
        ["H", "O", "L", "A"],
        ["D", "A", "N", "I"],
        ["S", "W", "I", "F", "T"],
        ["X", "C", "O", "D", "E"]]
    
    var imagenes:[UIImage] = [
        UIImage(named: "Hangman-0")!,
        UIImage(named: "Hangman-1")!,
        UIImage(named: "Hangman-2")!,
        UIImage(named: "Hangman-3")!,
        UIImage(named: "Hangman-4")!,
        UIImage(named: "Hangman-5")!,
        UIImage(named: "Hangman-6")!]
    
    var palabraParaAdivinar:[String] = Array()
    
    var vidas:Int = 0
    
    //Se crea una palabra temporal que contiene tantos guiones como chars la palabra que se tiene que adivinar
    var palabraTemporal:[String]!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //Se selecciona una palabra de la lista de manera random
        var indexPalabra: Int = Int(arc4random_uniform(UInt32(palabras.count)))
        palabraParaAdivinar = palabras[indexPalabra]
        
        
        //Se pasa las palabra en guiones para asignarlo al Label
        var guiones:String = String()
        for(var pos = 0; pos<palabraParaAdivinar.count; pos++){
            guiones += "_ "
        }
        
        palabraLabel.text = guiones
        
        //Se llena la palabra temporal de guiones
        palabraTemporal = palabraParaAdivinar
        for(var pos = 0; pos<palabraParaAdivinar.count; pos++){
            palabraTemporal[pos] = String("_ ")
        }
    }
    
    @IBAction func comprobarLetra(sender: UITextField) {
        var letraEscrita:String = String(letraTextField.text.uppercaseString)
        var existe:Bool = false
        //Se comprueba si la palabra existe
        for(var pos=0; pos<palabraParaAdivinar.count; pos++){
            if(palabraParaAdivinar[pos] == letraEscrita){
                existe = true
                palabraTemporal[pos] = letraEscrita
            }
        }
        
        var palabraASustituir:String = String()
        for(var pos = 0; pos<palabraTemporal.count; pos++){
            palabraASustituir += String(palabraTemporal[pos])
        }
        
        palabraLabel.text = palabraASustituir
        
        
        if(existe == false && vidas<imagenes.count){
            imagen.image = imagenes[vidas]
            vidas++
            
            if (vidas==imagenes.count) {
                var alert: UIAlertController = UIAlertController(
                    title: "PERDISTE",
                    message: "PERDISTE",
                    preferredStyle: UIAlertControllerStyle.Alert)
                
                alert.addAction(UIAlertAction(title: "Volver a Jugar",
                    style: UIAlertActionStyle.Default,
                    handler: volverAJugar))
                
                self.presentViewController(alert, animated: true, completion: nil)
            }
        }
        
        if(palabraParaAdivinar == palabraTemporal){
            var alert: UIAlertController = UIAlertController(
                title: "GANASTE",
                message: "GANASTE",
                preferredStyle: UIAlertControllerStyle.Alert)
            
            alert.addAction(UIAlertAction(title: "Volver a Jugar",
                style: UIAlertActionStyle.Default,
                handler: volverAJugar))
            
            self.presentViewController(alert, animated: true, completion: nil)
        }
        
        letraTextField.text = nil
    }
    
    func volverAJugar(alert: UIAlertAction!){
        //Se selecciona una palabra de la lista de manera random
        var indexPalabra: Int = Int(arc4random_uniform(UInt32(palabras.count)))
        palabraParaAdivinar = palabras[indexPalabra]
        
        
        //Se pasa las palabra en guiones para asignarlo al Label
        var guiones:String = String()
        for(var pos = 0; pos<palabraParaAdivinar.count; pos++){
            guiones += "_ "
        }
        
        palabraLabel.text = guiones
        
        //Se llena la palabra temporal de guiones
        palabraTemporal = palabraParaAdivinar
        for(var pos = 0; pos<palabraParaAdivinar.count; pos++){
            palabraTemporal[pos] = String("_ ")
        }
        
        imagen.image = nil
        
        vidas = 0
    }
    
    override func touchesBegan(touches: NSSet, withEvent event: UIEvent) {
        self.view.endEditing(true)
    }
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

